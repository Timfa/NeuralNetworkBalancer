﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BalancerManager : MonoBehaviour
{
    public int Generation = 0;
    public int AmountOfBalancers = 10;

    public float DistanceBetweenBalancers = 3;
    public int BalancersPerRow = 10;

    public Balancer prefab;

    public List<Balancer> Balancers = new List<Balancer>();

    private NeuralNetwork.Network best = null;
    private float bestLife = 0;

    public Vector2 bestOutput = new Vector2();

    public float ShakeTime = 5;
    private float shakeTimer = 5;

    public Renderer FloorRenderer;
    private Color floorColor;

	// Use this for initialization
	void Start ()
    {
        CreateBalancers();
        floorColor = FloorRenderer.material.color;
	}
	
	// Update is called once per frame
	void Update ()
    {
        shakeTimer -= Time.deltaTime;
        
        bool atLeastOneAlive = false;

        Vector3 totalPos = Vector3.zero;
        int totalAlive = 0;

        foreach(Balancer balancer in Balancers)
        {
            if(balancer.Alive)
            {
                if(shakeTimer <= 0)
                {
                    Vector2 force = Random.insideUnitCircle;

                    force.Normalize();

                    force *= 5000f;

                    balancer.Foot.AddForce(new Vector3(force.x, 0, force.y));
                }

                atLeastOneAlive = true;

                if(best == null || balancer.LifeTime > best.Fitness)
                {
                    best = balancer.Brain;
                }

                if(balancer.LifeTime > bestLife)
                    balancer.Best = true;

                totalPos += balancer.Foot.position;
                totalAlive++;
            }
            else if(shakeTimer <= 0)
            {
                foreach(Rigidbody rb in balancer.gameObject.GetComponentsInChildren<Rigidbody>())
                {
                    Vector3 force = Random.onUnitSphere;
                    
                    force *= 500f * rb.mass;

                    force.y = Mathf.Abs(force.y);

                    rb.useGravity = true;

                    rb.constraints = RigidbodyConstraints.None;

                    rb.AddForce(new Vector3(force.x, force.y, force.z));
                    
                    force = Random.onUnitSphere;

                    force *= 500f * rb.mass;
                    
                    rb.AddTorque(new Vector3(force.x, force.y, force.z));
                }
            }
        }
        
        if(shakeTimer <= 0)
        {
            FloorRenderer.material.color = new Color(1, 0, 1);
            shakeTimer = ShakeTime;
        }
        else
        {
            FloorRenderer.material.color = Color.Lerp(FloorRenderer.material.color, floorColor, 0.1f);
        }

        if(totalAlive > 0)
        {
            totalPos /= totalAlive;
            Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, totalPos + new Vector3(-10, 10, -10), 0.1f);
        }

        if (!atLeastOneAlive)
        {
            NeuralNetwork.Network bestNetwork = best;

            bestLife = (float)best.Fitness;

            CreateBalancers();

            foreach(Balancer balancer in Balancers)
            {
                balancer.Brain = bestNetwork.Clone();
                balancer.Brain.Mutate(5);
            }
        }
    }

    void OnGUI()
    {
        GUI.Box(new Rect(0, 0, 120, 30), "Generation " + Generation);
    }

    void CreateBalancers()
    {
        Generation++;

        if(Balancers.Count > 0)
        {
            foreach(Balancer balancer in Balancers)
                Destroy(balancer.gameObject);

            Balancers = new List<Balancer>();
        }

        for (int i = 0; i < AmountOfBalancers; i++)
        {
            float row = Mathf.CeilToInt(i / BalancersPerRow) * DistanceBetweenBalancers;
            float xPos = (i % BalancersPerRow) * DistanceBetweenBalancers;

            Balancer balancer = (Balancer)Instantiate(prefab);

            balancer.transform.position = new Vector3(xPos, 0, row);

            Vector2 force = Random.insideUnitCircle;

            force.Normalize();

            force *= 2000f;

            balancer.Foot.AddForce(new Vector3(force.x, 0, force.y));

            Balancers.Add(balancer);
        }
    }
}
