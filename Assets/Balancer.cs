﻿using UnityEngine;
using System.Collections;

public class Balancer : MonoBehaviour
{
    public NeuralNetwork.Network Brain;
    public Rigidbody Foot;
    public Transform PoleSphere;

    public float LifeTime
    {
        get
        {
            return Brain != null? (float)Brain.Fitness : 0;
        }

        set
        {
            Brain.Fitness = value;
        }
    }

    public bool Alive = true;
    public bool Best = false;

	// Use this for initialization
	void Start ()
    {
	
	}

    void SetupNetwork()
    {
        Brain = new NeuralNetwork.Network(2);
        Brain.AddLayer(4, NeuralNetwork.Neuron.ActivationTypes.Linear);
        Brain.AddLayer(4, NeuralNetwork.Neuron.ActivationTypes.Linear);
        Brain.AddLayer(2, NeuralNetwork.Neuron.ActivationTypes.Linear);
    }
	
	// Update is called once per frame
	void Update ()
    {
        if(Brain == null)
            SetupNetwork();

        Renderer renderer = PoleSphere.GetComponent<Renderer>();
        
        renderer.material.color = new Color(1, 0, 0);

        if(Alive)
        {
            LifeTime += Time.deltaTime;

            if(PoleSphere.position.y < 2 || Vector3.Distance(Foot.position, Vector3.zero) > 1000)
            {
                Alive = false;
                ConfigurableJoint joint = gameObject.GetComponentInChildren<ConfigurableJoint>();
                Destroy(joint);
            }
            double[] forces = Brain.GetOutput(new double[] { PoleSphere.position.x - Foot.position.x, PoleSphere.position.z - Foot.position.z });

            Foot.AddForce(new Vector3(Mathf.Clamp((float)forces[0], -300, 300), 0, Mathf.Clamp((float)forces[1], -300, 300)));

            renderer.material.color = Best? new Color(0, 1, 0) : new Color(1, 1, 0);
        }
    }
}
